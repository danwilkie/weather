STRUCTURE:The app is split into 3 modules:

app - the main Android app module containing activities, views, adapters and so on. This is also
the place for UI tests.

core - a pure Java module containing core model objects and business logic, including presenters.
This module contains JUnit tests.

network - a pure Java module containing Retrofit, network model objects (prefixed with Gson),
and network services. This module contains JUnit tests.

In each module, there is a "common" package for any common classes. Otherwise, classes are packaged
based on their feature / screen.


TRADE-OFFS (generally due to time constraints)

The "Current weather" card is only an approximation of the current weather, taken as the first of
the three-hour weather snapshots returned by the endpoint. If we required a more accurate
"current weather" reading, we could call a separate endpoint to get this, however I considered that
out-of-scope for this task.

The design is not optimised for tablets. This could be achieved by allowing 2 columns in the
RecyclerView instead of one once a certain screenWidth has been reached.

The app caches the downloaded data in memory, but not to disk. The cached data will survive through
screen rotations. However, once the app is destroyed, the data is gone.