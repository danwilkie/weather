package dan.wilkie.weather.forecast;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import dan.wilkie.weather.R;
import dan.wilkie.weather.app.ScopeActivity;

public class ForecastActivity extends ScopeActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_forecast);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }
}
