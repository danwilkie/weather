package dan.wilkie.weather.forecast;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import dan.wilkie.weather.R;
import dan.wilkie.weather.common.DataAdapter;
import dan.wilkie.weather.common.HeaderFooterAdapter;
import dan.wilkie.weather.common.ImageLoader;
import dan.wilkie.weather.common.MessageDisplayer;
import rx.Observable;

import static com.jakewharton.rxbinding.support.v4.widget.RxSwipeRefreshLayout.refreshes;
import static dan.wilkie.weather.dependency.ForecastModule.provideForecastPresenter;

public class ForecastView extends LinearLayout implements ForecastPresenter.View {
    private final ForecastPresenter presenter = provideForecastPresenter(getContext());

    private final DataAdapter<DailyForecast, DailyForecastViewHolder> adapter =
            new DataAdapter<>(R.layout.row_daily_forecast, DailyForecastViewHolder::new);

    private final MessageDisplayer messageDisplayer = new MessageDisplayer(this);
    private final ImageLoader imageLoader = new ImageLoader();

    private SwipeRefreshLayout swipeRefreshLayout;

    private RecyclerView recyclerView;
    private ProgressBar progressBar;

    public ForecastView(Context context) {
        super(context);
    }

    public ForecastView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ForecastView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        setupRecyclerView();
    }

    private void setupRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
    }

    private View createHeaderView(ViewGroup parent, ThreeHourForecast currentWeather) {
        View headerView = LayoutInflater.from(parent.getContext()).inflate(R.layout.current_weather_header, parent, false);

        TextView currentWeatherTextView = (TextView) headerView.findViewById(R.id.currentWeatherTextView);
        ImageView currentWeatherIconView = (ImageView) headerView.findViewById(R.id.currentWeatherIconView);

        currentWeatherTextView.setText(getResources().getString(R.string.current_temp, (int) currentWeather.getTemp()));
        imageLoader.loadWeatherIcon(currentWeather.getIcon(), currentWeatherIconView);

        return headerView;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        presenter.attach(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        presenter.detach(this);
    }

    @Override
    public Observable<Void> onRefresh() {
        return refreshes(swipeRefreshLayout);
    }

    @Override
    public void showLoading(boolean show) {
        if (!swipeRefreshLayout.isRefreshing()) {
            progressBar.setVisibility(show ? VISIBLE : GONE);
        } else {
            swipeRefreshLayout.setRefreshing(show);
        }
    }

    @Override
    public void showForecast(List<DailyForecast> forecast) {
        adapter.setItems(forecast);

        /* Here we take the first 3-hour forecast and use it to approximate the "current weather".
            Alternatively, a separate endpoint could be called to get the current weather. */
        ThreeHourForecast currentWeather = forecast.get(0).getForecasts().get(0);
        recyclerView.setAdapter(HeaderFooterAdapter.withHeader(adapter, viewGroup -> createHeaderView(viewGroup, currentWeather)));
    }

    @Override
    public void showError() {
        messageDisplayer.displayMessage(R.string.generic_error);
    }
}
