package dan.wilkie.weather.forecast;

import android.content.res.Resources;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import dan.wilkie.weather.R;
import dan.wilkie.weather.common.DataAdapter;

import static android.support.v7.widget.LinearLayoutManager.HORIZONTAL;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class DailyForecastViewHolder extends DataAdapter.DataViewHolder<DailyForecast> {
    private final TextView dayTextView;
    private final TextView minMaxTextView;
    private final ImageView umbrellaIcon;
    private final ImageView sunglassesIcon;
    private final RecyclerView dayRecyclerView;

    private final DataAdapter<ThreeHourForecast, ThreeHourForecastViewHolder> adapter =
            new DataAdapter<>(R.layout.row_three_hour_forecast, ThreeHourForecastViewHolder::new);

    public DailyForecastViewHolder(View itemView) {
        super(itemView);
        dayTextView = (TextView) itemView.findViewById(R.id.dayTextView);
        minMaxTextView = (TextView) itemView.findViewById(R.id.minMaxTextView);
        umbrellaIcon = (ImageView) itemView.findViewById(R.id.umbrellaIcon);
        sunglassesIcon = (ImageView) itemView.findViewById(R.id.sunglassesIcon);
        dayRecyclerView = (RecyclerView) itemView.findViewById(R.id.dayRecyclerView);
        setupRecyclerView();
    }

    private void setupRecyclerView() {
        dayRecyclerView.setLayoutManager(new LinearLayoutManager(dayRecyclerView.getContext(), HORIZONTAL, false));
        dayRecyclerView.setAdapter(adapter);
    }

    @Override
    protected void bind(DailyForecast data) {
        Resources res = dayTextView.getContext().getResources();
        dayTextView.setText(data.getDay());
        minMaxTextView.setText(res.getString(R.string.min_max, (int) data.getMinTemp(), (int) data.getMaxTemp()));
        umbrellaIcon.setVisibility(data.isUmbrellaNeeded() ? VISIBLE : GONE);
        sunglassesIcon.setVisibility(data.isSunglassesNeeded() ? VISIBLE : GONE);
        adapter.setItems(data.getForecasts());
    }
}
