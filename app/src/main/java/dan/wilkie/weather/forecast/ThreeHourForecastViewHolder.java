package dan.wilkie.weather.forecast;

import android.content.res.Resources;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import dan.wilkie.weather.R;
import dan.wilkie.weather.common.DataAdapter;
import dan.wilkie.weather.common.ImageLoader;

import static java.util.concurrent.TimeUnit.SECONDS;

public class ThreeHourForecastViewHolder extends DataAdapter.DataViewHolder<ThreeHourForecast> {
    private final ImageView weatherImageView;
    private final TextView timeTextView;
    private final TextView tempTextView;
    private final TextView summaryTextView;

    // hard-coded to UK as app only supports London for now
    private final DateFormat timeFormat = new SimpleDateFormat("HH:mm",Locale.UK);
    private final ImageLoader imageLoader = new ImageLoader();

    public ThreeHourForecastViewHolder(View itemView) {
        super(itemView);
        weatherImageView = (ImageView) itemView.findViewById(R.id.weatherImageView);
        timeTextView = (TextView) itemView.findViewById(R.id.timeTextView);
        tempTextView = (TextView) itemView.findViewById(R.id.tempTextView);
        summaryTextView = (TextView) itemView.findViewById(R.id.summaryTextView);
    }

    @Override
    protected void bind(ThreeHourForecast data) {
        Resources res = weatherImageView.getContext().getResources();
        imageLoader.loadWeatherIcon(data.getIcon(), weatherImageView);
        timeTextView.setText(timeFormat.format(new Date(SECONDS.toMillis(data.getTimestamp()))));
        tempTextView.setText(res.getString(R.string.temp, (int) data.getTemp()));
        summaryTextView.setText(data.getWeatherDescription());
    }

}
