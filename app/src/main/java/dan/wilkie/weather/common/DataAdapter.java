package dan.wilkie.weather.common;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Func1;

/**
 * A generic adapter that can be used to display data of a given type
 * @param <T> The type of data to be displayed
 * @param <VH> The type of ViewHolder, which must support the data type T
 */
public class DataAdapter<T, VH extends DataAdapter.DataViewHolder<T>> extends RecyclerView.Adapter<VH> {
    private final int layoutId;
    private final Func1<View, VH> holderCreator;
    private final List<T> items = new ArrayList<>();

    public DataAdapter(int layoutId, Func1<View, VH> holderCreator) {
        this.layoutId = layoutId;
        this.holderCreator = holderCreator;
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(this.layoutId, parent, false);
        return this.holderCreator.call(itemView);
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        holder.bind(getItem(position));
    }

    public void setItems(List<T> items) {
        this.items.clear();
        this.items.addAll(items);
        this.notifyDataSetChanged();
    }

    public T getItem(int position) {
        return this.items.get(position);
    }

    public int getItemCount() {
        return this.items.size();
    }

    public abstract static class DataViewHolder<T> extends RecyclerView.ViewHolder {
        public DataViewHolder(View itemView) {
            super(itemView);
        }

        protected abstract void bind(T data);
    }
}