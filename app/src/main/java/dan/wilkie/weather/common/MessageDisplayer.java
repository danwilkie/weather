package dan.wilkie.weather.common;


import android.support.design.widget.Snackbar;
import android.view.View;

import static android.support.design.widget.Snackbar.LENGTH_LONG;

public class MessageDisplayer {
    private final View view;

    public MessageDisplayer(View view) {
        this.view = view;
    }

    public void displayMessage(int resourceId) {
        Snackbar.make(view, resourceId, LENGTH_LONG);
    }
}
