package dan.wilkie.weather.common;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import rx.functions.Func1;

/**
 * Adapter that supports adding a header or footer (or both) to a provided RecyclerView adapter.
 * Taken from a library of mine that I use for several projects.
 *
 * @param <VH>
 */
public class HeaderFooterAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int HEADER = 1001;
    private static final int FOOTER = 1002;

    private final Func1<ViewGroup, View> headerCreator;
    private final Func1<ViewGroup, View> footerCreator;

    private RecyclerView.Adapter<VH> wrapped;

    public static <VH extends RecyclerView.ViewHolder> HeaderFooterAdapter<VH> withHeader(RecyclerView.Adapter<VH> wrapped, Func1<ViewGroup, View> headerCreator) {
        return new HeaderFooterAdapter<>(wrapped, headerCreator, null);
    }

    public static <VH extends RecyclerView.ViewHolder> HeaderFooterAdapter<VH> withFooter(RecyclerView.Adapter<VH> wrapped, Func1<ViewGroup, View> footerCreator) {
        return new HeaderFooterAdapter<>(wrapped, null, footerCreator);
    }

    public static <VH extends RecyclerView.ViewHolder> HeaderFooterAdapter<VH> withHeaderAndFooter(RecyclerView.Adapter<VH> wrapped, Func1<ViewGroup, View> headerCreator, Func1<ViewGroup, View> footerCreator) {
        return new HeaderFooterAdapter<>(wrapped, headerCreator, footerCreator);
    }

    private HeaderFooterAdapter(RecyclerView.Adapter<VH> wrapped, Func1<ViewGroup, View> headerCreator, Func1<ViewGroup, View> footerCreator) {
        this.wrapped = wrapped;
        this.headerCreator = headerCreator;
        this.footerCreator = footerCreator;

        wrapped.registerAdapterDataObserver(
                new RecyclerView.AdapterDataObserver() {
                    @Override
                    public void onChanged() {
                        notifyDataSetChanged();
                    }

                    @Override
                    public void onItemRangeInserted(int positionStart, int itemCount) {
                        notifyItemRangeInserted(positionInThisAdapter(positionStart), itemCount);
                    }

                    @Override
                    public void onItemRangeChanged(int positionStart, int itemCount) {
                        notifyItemRangeChanged(positionInThisAdapter(positionStart), itemCount);
                    }

                    @Override
                    public void onItemRangeChanged(int positionStart, int itemCount, Object payload) {
                        notifyItemRangeChanged(positionInThisAdapter(positionStart), itemCount, payload);
                    }

                    @Override
                    public void onItemRangeRemoved(int positionStart, int itemCount) {
                        notifyItemRangeRemoved(positionInThisAdapter(positionStart), itemCount);
                    }
                });
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEADER) {
            return new ViewHolder(headerCreator.call(parent));
        } else if (viewType == FOOTER) {
            return new ViewHolder(footerCreator.call(parent));
        } else {
            return wrapped.onCreateViewHolder(parent, viewType);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (!isHeader(position) && !isFooter(position)) {
            wrapped.onBindViewHolder((VH) holder, positionInWrappedAdapter(position));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return hasHeader() && position == 0 ? HEADER :
                hasFooter() && position == getItemCount() - 1 ? FOOTER :
                        wrapped.getItemViewType(positionInWrappedAdapter(position));
    }

    @Override
    public int getItemCount() {
        return wrapped.getItemCount() + (hasHeader() ? 1 : 0) + (hasFooter() ? 1 : 0);
    }

    @Override
    public long getItemId(int position) {
        return isHeader(position) ? -1 :
                isFooter(position) ? -2 :
                        wrapped.getItemId(positionInWrappedAdapter(position));
    }

    private boolean hasFooter() {
        return footerCreator != null;
    }

    private boolean hasHeader() {
        return headerCreator != null;
    }

    private int positionInWrappedAdapter(int positionInHeaderFooterAdapter) {
        return hasHeader() ? positionInHeaderFooterAdapter - 1 : positionInHeaderFooterAdapter;
    }

    private int positionInThisAdapter(int positionInWrappedAdapter) {
        return hasHeader() ? positionInWrappedAdapter + 1 : positionInWrappedAdapter;
    }

    private boolean isHeader(int position) {
        return getItemViewType(position) == HEADER;
    }

    private boolean isFooter(int position) {
        return getItemViewType(position) == FOOTER;
    }

    private static class ViewHolder extends RecyclerView.ViewHolder {
        ViewHolder(View itemView) {
            super(itemView);
        }
    }
}