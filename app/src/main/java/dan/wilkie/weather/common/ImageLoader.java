package dan.wilkie.weather.common;

import android.widget.ImageView;

import com.bumptech.glide.Glide;

import dan.wilkie.weather.R;

public class ImageLoader {
    private static final String ICON_URL_FORMAT = "http://openweathermap.org/img/w/%s.png";

    public void loadWeatherIcon(String iconId, ImageView imageView) {
        loadImage(String.format(ICON_URL_FORMAT, iconId), imageView);
    }

    private void loadImage(String url, ImageView imageView) {
        Glide.with(imageView.getContext())
                .load(url)
                .placeholder(R.drawable.circle)
                .into(imageView);
    }
}
