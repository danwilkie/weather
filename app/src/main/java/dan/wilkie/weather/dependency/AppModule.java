package dan.wilkie.weather.dependency;

import com.memoizrlabs.ShankModule;

import dan.wilkie.weather.api.RetrofitFactory;
import dan.wilkie.weather.api.WeatherApi;
import dan.wilkie.weather.common.RxThreadCallAdapterFactory;
import retrofit2.Retrofit;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.memoizrlabs.Shank.provideSingleton;
import static com.memoizrlabs.Shank.registerFactory;

/**
 * Provides dependencies required across the app (rather than just for one screen/ feature)
 **/
public class AppModule implements ShankModule {

    @Override
    public void registerFactories() {
        registerWeatherApi();
    }

    private void registerWeatherApi() {
        registerFactory(WeatherApi.class, () -> {
            RxThreadCallAdapterFactory callAdapterFactory = new RxThreadCallAdapterFactory(Schedulers.io(), AndroidSchedulers.mainThread());
            Retrofit retrofit = new RetrofitFactory().createRetrofit(callAdapterFactory);
            return retrofit.create(WeatherApi.class);
        });
    }

    public static WeatherApi provideWeatherApi() {
        return provideSingleton(WeatherApi.class);
    }
}
