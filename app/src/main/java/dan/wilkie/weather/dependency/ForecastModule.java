package dan.wilkie.weather.dependency;

import android.content.Context;

import com.memoizrlabs.ShankModule;

import dan.wilkie.weather.forecast.ForecastPresenter;
import dan.wilkie.weather.forecast.ForecastRepository;
import dan.wilkie.weather.forecast.RetrofitForecastService;

import static com.memoizrlabs.Shank.provideNew;
import static com.memoizrlabs.Shank.registerFactory;
import static dan.wilkie.weather.app.ScopeActivity.provideActivityScopedSingleton;
import static dan.wilkie.weather.dependency.AppModule.provideWeatherApi;

/**
 * Provides dependencies for the Forecast screen / feature.
 **/
public class ForecastModule implements ShankModule {

    @Override
    public void registerFactories() {
        registerForecastRepository();
        registerForecastPresenter();
    }

    private void registerForecastRepository() {
        registerFactory(ForecastRepository.class, () ->
                new ForecastRepository(new RetrofitForecastService(provideWeatherApi())));
    }

    private void registerForecastPresenter() {
        registerFactory(ForecastPresenter.class, () ->
                new ForecastPresenter(provideNew(ForecastRepository.class)));
    }

    public static ForecastPresenter provideForecastPresenter(Context context) {
        return provideActivityScopedSingleton(context, ForecastPresenter.class);
    }
}
