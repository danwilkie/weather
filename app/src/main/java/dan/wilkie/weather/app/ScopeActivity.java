package dan.wilkie.weather.app;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;

import com.memoizrlabs.Scope;
import com.memoizrlabs.Shank;

import java.util.UUID;

/**
 * A base activity class that maintains a scope id across configuration changes.
 *
 * This scope id can be used in combination with the Shank library to provide dependencies
 * (for example presenters and repositories) that will survive configuration changes,
 * and only be destroyed when the activity is "truly" destroyed (e.g. finish() is called or
 * the user presses the back button to destroy the activity).
 *
 * This allows data to be retained when the screen is rotated, so that we don't need to make a
 * fresh call to the server.
 */
public class ScopeActivity extends AppCompatActivity {
    private static final String SCOPE_ID = "SCOPE_ID";
    private UUID scopeId;

    public ScopeActivity() {
    }

    public static Scope activityScope(Context context) {
        return Scope.scope(toScopeActivity(context).scopeId);
    }

    public static ScopeActivity toScopeActivity(Context context) {
        if(context instanceof Activity) {
            return (ScopeActivity)context;
        } else if(context instanceof ContextThemeWrapper) {
            return toScopeActivity(((ContextThemeWrapper)context).getBaseContext());
        } else if(context instanceof android.view.ContextThemeWrapper) {
            return toScopeActivity(((android.view.ContextThemeWrapper)context).getBaseContext());
        } else if(context instanceof ContextWrapper) {
            return toScopeActivity(((ContextWrapper)context).getBaseContext());
        } else {
            throw new IllegalArgumentException("Context " + context + " is not an activity.");
        }
    }

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null) {
            this.scopeId = UUID.randomUUID();
        } else {
            this.scopeId = (UUID)savedInstanceState.getSerializable(SCOPE_ID);
        }

    }

    protected void onDestroy() {
        super.onDestroy();
        if(this.isFinishing()) {
            // The activity is being truly destroyed - so it's time to destroy any scoped objects
            Scope.scope(this.scopeId).clear();
        }
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("SCOPE_ID", this.scopeId);
    }

    /**
     * Provides an instance of the specified class that will survive as long as the current
     * activity exists (also survives configuration changes).
     * @param scopeActivityContext Context which can be used to get a reference to the activity
     * @param cls The class to be instantiated
     * @param <T> The type to be instantiated
     * @return the instance, which may be new or may already have existed
     */
    public static <T> T provideActivityScopedSingleton(Context scopeActivityContext, Class<T> cls) {
        return Shank.with(activityScope(scopeActivityContext)).provideSingleton(cls);
    }
}
