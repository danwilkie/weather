package dan.wilkie.weather.app;

import android.app.Application;

import dan.wilkie.weather.dependency.AppModule;
import dan.wilkie.weather.dependency.ForecastModule;

import static com.memoizrlabs.ShankModuleInitializer.initializeModules;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        initializeModules(new AppModule(), new ForecastModule());
    }
}
