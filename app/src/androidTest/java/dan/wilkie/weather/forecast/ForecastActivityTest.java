package dan.wilkie.weather.forecast;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.startsWith;

@RunWith(AndroidJUnit4.class)
public class ForecastActivityTest {
    @Rule
    public ActivityTestRule<ForecastActivity> activityRule = new ActivityTestRule<>(ForecastActivity.class);

    @Test
    public void showsCurrentWeather() throws Exception {
        onView(withText(startsWith("Current temp in London: "))).check(matches(isDisplayed()));
    }
}
