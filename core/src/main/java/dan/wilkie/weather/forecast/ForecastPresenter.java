package dan.wilkie.weather.forecast;

import com.mvcoding.mvp.Presenter;

import java.util.List;

import rx.Observable;

/**
 * Presenter for the "Forecast" screen, following the Reactive MVP approach described at
 * https://medium.com/@mvarnagiris/reactive-mvp-part-1-b751ce3e3246
 *
 * View interactions (in this case, swipe to refresh) are exposed as observables that can be subscribed to.
 * Similarly, updates from the repository are also exposed as observables.
 *
 * When the view is detached, all subscriptions are automatically unsubscribed (handled in the base Presenter class).
 */
public class ForecastPresenter extends Presenter<ForecastPresenter.View> {
    private final ForecastRepository forecastRepository;

    public ForecastPresenter(ForecastRepository forecastRepository) {
        this.forecastRepository = forecastRepository;
    }

    @Override
    protected void onViewAttached(View view) {
        super.onViewAttached(view);
        unsubscribeOnDetach(forecastRepository.observeLoading().subscribe(view::showLoading));
        unsubscribeOnDetach(forecastRepository.observeErrors().subscribe(error -> view.showError()));
        unsubscribeOnDetach(forecastRepository.observeData().subscribe(view::showForecast));
        unsubscribeOnDetach(view.onRefresh().subscribe(refresh -> forecastRepository.refresh()));
    }

    public interface View extends Presenter.View {
        Observable<Void> onRefresh();

        void showLoading(boolean show);

        void showForecast(List<DailyForecast> forecast);

        void showError();
    }
}
