package dan.wilkie.weather.forecast;

import java.util.List;

import rx.Observable;

public interface ForecastService {
    Observable<List<DailyForecast>> getLondonForecast();
}
