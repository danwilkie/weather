package dan.wilkie.weather.forecast;

class ThreeHourForecast {
    private final double temp;
    private final int weatherId;
    private final String weatherTitle;
    private final String weatherDescription;
    private final String icon;
    private final long timestamp;

    public ThreeHourForecast(double temp, int weatherId, String weatherTitle, String weatherDescription, String icon, long timestamp) {
        this.temp = temp;
        this.weatherId = weatherId;
        this.weatherTitle = weatherTitle;
        this.weatherDescription = weatherDescription;
        this.icon = icon;
        this.timestamp = timestamp;
    }

    public double getTemp() {
        return temp;
    }

    public int getWeatherId() {
        return weatherId;
    }

    public String getWeatherTitle() {
        return weatherTitle;
    }

    public String getWeatherDescription() {
        return weatherDescription;
    }

    public String getIcon() {
        return icon;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ThreeHourForecast that = (ThreeHourForecast) o;

        if (Double.compare(that.temp, temp) != 0) return false;
        if (weatherId != that.weatherId) return false;
        if (timestamp != that.timestamp) return false;
        if (weatherTitle != null ? !weatherTitle.equals(that.weatherTitle) : that.weatherTitle != null)
            return false;
        if (weatherDescription != null ? !weatherDescription.equals(that.weatherDescription) : that.weatherDescription != null)
            return false;
        return icon != null ? icon.equals(that.icon) : that.icon == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp1;
        temp1 = Double.doubleToLongBits(temp);
        result = (int) (temp1 ^ (temp1 >>> 32));
        result = 31 * result + weatherId;
        result = 31 * result + (weatherTitle != null ? weatherTitle.hashCode() : 0);
        result = 31 * result + (weatherDescription != null ? weatherDescription.hashCode() : 0);
        result = 31 * result + (icon != null ? icon.hashCode() : 0);
        result = 31 * result + (int) (timestamp ^ (timestamp >>> 32));
        return result;
    }
}
