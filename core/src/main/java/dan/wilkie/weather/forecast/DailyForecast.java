package dan.wilkie.weather.forecast;

import java.util.List;

public class DailyForecast {
    private final String day;
    private final double minTemp;
    private final double maxTemp;
    private final List<ThreeHourForecast> forecasts;
    private final boolean umbrellaNeeded;
    private final boolean sunglassesNeeded;

    public DailyForecast(String day, double minTemp, double maxTemp, List<ThreeHourForecast> forecasts, boolean umbrellaNeeded, boolean sunglassesNeeded) {
        this.day = day;
        this.minTemp = minTemp;
        this.maxTemp = maxTemp;
        this.forecasts = forecasts;
        this.umbrellaNeeded = umbrellaNeeded;
        this.sunglassesNeeded = sunglassesNeeded;
    }

    public String getDay() {
        return day;
    }

    public double getMinTemp() {
        return minTemp;
    }

    public double getMaxTemp() {
        return maxTemp;
    }

    public List<ThreeHourForecast> getForecasts() {
        return forecasts;
    }

    public boolean isUmbrellaNeeded() {
        return umbrellaNeeded;
    }

    public boolean isSunglassesNeeded() {
        return sunglassesNeeded;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DailyForecast that = (DailyForecast) o;

        if (Double.compare(that.minTemp, minTemp) != 0) return false;
        if (Double.compare(that.maxTemp, maxTemp) != 0) return false;
        if (umbrellaNeeded != that.umbrellaNeeded) return false;
        if (sunglassesNeeded != that.sunglassesNeeded) return false;
        if (day != null ? !day.equals(that.day) : that.day != null) return false;
        return forecasts != null ? forecasts.equals(that.forecasts) : that.forecasts == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = day != null ? day.hashCode() : 0;
        temp = Double.doubleToLongBits(minTemp);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(maxTemp);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (forecasts != null ? forecasts.hashCode() : 0);
        result = 31 * result + (umbrellaNeeded ? 1 : 0);
        result = 31 * result + (sunglassesNeeded ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DailyForecast{" +
                "day='" + day + '\'' +
                ", minTemp=" + minTemp +
                ", maxTemp=" + maxTemp +
                ", forecasts=" + forecasts +
                ", umbrellaNeeded=" + umbrellaNeeded +
                ", sunglassesNeeded=" + sunglassesNeeded +
                '}';
    }
}
