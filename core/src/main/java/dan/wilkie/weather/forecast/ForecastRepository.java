package dan.wilkie.weather.forecast;

import java.util.List;

import dan.wilkie.weather.common.Repository;

public class ForecastRepository extends Repository<List<DailyForecast>> {
    public ForecastRepository(ForecastService forecastService) {
        super(forecastService::getLondonForecast);
    }
}
