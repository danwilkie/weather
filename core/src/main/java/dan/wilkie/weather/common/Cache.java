package dan.wilkie.weather.common;

import rx.Observable;

public interface Cache<T> {
    void write(T data);

    Observable<T> observe();

    boolean isEmpty();
}
