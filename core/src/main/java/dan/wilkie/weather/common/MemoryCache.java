package dan.wilkie.weather.common;

import rx.Observable;
import rx.subjects.PublishSubject;

public class MemoryCache<T> implements Cache<T> {
    private T data = null;
    private PublishSubject<T> dataSubject = PublishSubject.create();

    @Override
    public void write(T data) {
        this.data = data;
        dataSubject.onNext(data);
    }

    @Override
    public Observable<T> observe() {
        return (data == null) ? dataSubject : dataSubject.startWith(data);
    }

    @Override
    public boolean isEmpty() {
        return data == null;
    }
}
