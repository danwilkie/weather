package dan.wilkie.weather.common;

import rx.Observable;
import rx.functions.Func0;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;

import static rx.Observable.empty;

/**
 * Repository - taken form a library of mine that I use for several projects.
 *
 * Provides an observable view to underlying data (typically fetched from the network).
 * Observers can observe the data itself, the state of loading, and any errors that occur while loading.
 * <p>
 * Once loaded, the data will be stored in the provided cache, so that future observers will be able to
 * observe the same data without a fresh load/network call being required.
 * <p>
 * The cache could be an in-memory cache or a disk cache.
 *
 * @param <T> Type of data
 */
public class Repository<T> {
    private final Func0<Observable<T>> requestFunction;
    private final Cache<T> cache;
    private final PublishSubject<Throwable> errors = PublishSubject.create();
    private final BehaviorSubject<Boolean> loading = BehaviorSubject.create();

    public Repository(Func0<Observable<T>> requestFunction, Cache<T> cache) {
        this.requestFunction = requestFunction;
        this.cache = cache;
    }

    public Repository(Func0<Observable<T>> requestFunction) {
        this(requestFunction, new MemoryCache<>());
    }

    public Observable<T> observeData() {
        if (cache.isEmpty() && !isCurrentlyLoading()) {
            refresh();
        }
        return cache.observe();
    }

    public Observable<Throwable> observeErrors() {
        return errors.asObservable();
    }

    public Observable<Boolean> observeLoading() {
        return loading.asObservable();
    }

    public void refresh() {
        requestFunction.call()
                .take(1)
                .doOnError((v) -> {
                    errors.onNext(v);
                    loading.onNext(false);
                })
                .onErrorResumeNext(it -> empty())
                .doOnSubscribe(() -> loading.onNext(true))
                .subscribe((data) -> {
                    cache.write(data);
                    loading.onNext(false);
                });
    }

    private boolean isCurrentlyLoading() {
        return loading.hasValue() && loading.getValue();
    }
}
