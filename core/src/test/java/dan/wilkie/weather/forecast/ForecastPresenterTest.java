package dan.wilkie.weather.forecast;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;

import java.util.List;

import rx.subjects.PublishSubject;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static rx.Observable.error;
import static rx.Observable.just;

public class ForecastPresenterTest {
    private ForecastService forecastService = mock(ForecastService.class);
    private ForecastPresenter presenter = new ForecastPresenter(new ForecastRepository(forecastService));
    private ForecastPresenter.View view = mock(ForecastPresenter.View.class);
    private ForecastPresenter.View view2 = mock(ForecastPresenter.View.class);
    private InOrder inOrder = inOrder(view, view2);
    private List<DailyForecast> forecast = singletonList(new DailyForecast("Monday", 1.0, 2.0, emptyList(), false, false));
    private List<DailyForecast> forecast2 = singletonList(new DailyForecast("Tuesday", 3.0, 4.0, emptyList(), true, true));
    private Throwable throwable = new Throwable();
    private PublishSubject<Void> refreshSubject = PublishSubject.create();

    @Before
    public void setUp() {
        when(view.onRefresh()).thenReturn(refreshSubject);
        when(view2.onRefresh()).thenReturn(refreshSubject);
    }
    
    @Test
    public void showsDataWhenLoadSucceeds() {
        when(forecastService.getLondonForecast()).thenReturn(just(forecast));

        presenter.attach(view);

        verify(view).showLoading(true);
        verify(view).showLoading(false);
        verify(view).showForecast(forecast);
    }

    @Test
    public void showsErrorWhenLoadFails() {
        when(forecastService.getLondonForecast()).thenReturn(error(throwable));

        presenter.attach(view);

        verify(view).showLoading(true);
        verify(view).showLoading(false);
        verify(view).showError();
    }

    @Test
    public void refreshesDataWhenRequested() {
        when(forecastService.getLondonForecast()).thenReturn(just(forecast), just(forecast2));

        presenter.attach(view);
        refreshSubject.onNext(null);

        verify(forecastService, times(2)).getLondonForecast();
        inOrder.verify(view).showLoading(true);
        inOrder.verify(view).showLoading(false);
        inOrder.verify(view).showForecast(forecast);
        inOrder.verify(view).showLoading(true);
        inOrder.verify(view).showForecast(forecast2);
        inOrder.verify(view).showLoading(false);
    }

    @Test
    public void showsCachedDataWhenNewViewIsAttached() {
        when(forecastService.getLondonForecast()).thenReturn(just(forecast));

        presenter.attach(view);
        presenter.detach(view);
        presenter.attach(view2);

        verify(view).showForecast(forecast);
        verify(view2).showForecast(forecast);
        verify(forecastService, times(1)).getLondonForecast();
    }
}