package dan.wilkie.weather.common;

import org.junit.Before;
import org.junit.Test;

import rx.Observable;
import rx.observers.TestSubscriber;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static rx.Observable.error;
import static rx.Observable.just;

public class RepositoryTest {
    private RemoteService remoteService = mock(RemoteService.class);
    private Throwable throwable = new Throwable();
    private TestSubscriber<String> dataSubscriber = TestSubscriber.create();
    private TestSubscriber<String> dataSubscriber2 = TestSubscriber.create();
    private TestSubscriber<Throwable> errorSubscriber = TestSubscriber.create();
    private TestSubscriber<Boolean> loadingSubscriber = TestSubscriber.create();
    private Repository<String> repository = new Repository<>(() -> remoteService.loadData());

    @Before
    public void setUp() {
        repository.observeErrors().subscribe(errorSubscriber);
        repository.observeLoading().subscribe(loadingSubscriber);
        when(remoteService.loadData()).thenReturn(just("data"));
    }

    @Test
    public void emitsDataWhenLoadSucceeds() {
        repository.observeData().subscribe(dataSubscriber);

        dataSubscriber.assertValues("data");
    }

    @Test
    public void cachesDataAndEmitsItToFutureSubscribers() {
        repository.observeData().subscribe(dataSubscriber);
        repository.observeData().subscribe(dataSubscriber2);

        dataSubscriber.assertValues("data");
        dataSubscriber2.assertValues("data");
        verify(remoteService, times(1)).loadData();
    }

    @Test
    public void refreshesDataWhenRequested() {
        when(remoteService.loadData()).thenReturn(just("data1"), just("data2"));

        repository.observeData().subscribe(dataSubscriber);
        repository.refresh();

        dataSubscriber.assertValues("data1", "data2");
        verify(remoteService, times(2)).loadData();
    }

    @Test
    public void emitsErrorWhenLoadFails() {
        when(remoteService.loadData()).thenReturn(error(throwable));

        repository.observeData().subscribe(dataSubscriber);

        errorSubscriber.assertValues(throwable);
    }

    @Test
    public void emitsLoadingUntilRequestCompletes() {
        repository.observeData().subscribe(dataSubscriber);

        loadingSubscriber.assertValues(true, false);
    }

    private interface RemoteService {
        Observable<String> loadData();
    }
}