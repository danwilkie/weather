package dan.wilkie.weather.common;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import rx.Observable;
import rx.Scheduler;

/**
 * Ensures that network requests are made on the io thread, and results are returned to the main thread.
 */
public class RxThreadCallAdapterFactory extends CallAdapter.Factory {
    private final Scheduler ioScheduler;
    private final Scheduler mainScheduler;
    private final RxJavaCallAdapterFactory rxFactory = RxJavaCallAdapterFactory.create();

    public RxThreadCallAdapterFactory(Scheduler ioScheduler, Scheduler mainScheduler) {
        this.ioScheduler = ioScheduler;
        this.mainScheduler = mainScheduler;
    }

    public CallAdapter<?> get(Type returnType, Annotation[] annotations, Retrofit retrofit) {
        CallAdapter rxCallAdapter = this.rxFactory.get(returnType, annotations, retrofit);
        return rxCallAdapter != null ? new RxThreadCallAdapter(rxCallAdapter) : null;
    }

    private class RxThreadCallAdapter implements CallAdapter<Observable<?>> {
        CallAdapter<Observable<?>> rxCallAdapter;

        RxThreadCallAdapter(CallAdapter<Observable<?>> rxCallAdapter) {
            this.rxCallAdapter = rxCallAdapter;
        }

        public Type responseType() {
            return this.rxCallAdapter.responseType();
        }

        public <T> Observable<?> adapt(Call<T> call) {
            return ((Observable) this.rxCallAdapter.adapt(call)).subscribeOn(ioScheduler).observeOn(mainScheduler);
        }
    }
}