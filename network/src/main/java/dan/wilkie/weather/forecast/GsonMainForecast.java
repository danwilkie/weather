package dan.wilkie.weather.forecast;

class GsonMainForecast {
    private final double temp;
    private final double tempMin;
    private final double tempMax;

    public GsonMainForecast(double temp, double tempMin, double tempMax) {
        this.temp = temp;
        this.tempMin = tempMin;
        this.tempMax = tempMax;
    }

    public double getTemp() {
        return temp;
    }

    public double getTempMin() {
        return tempMin;
    }

    public double getTempMax() {
        return tempMax;
    }
}
