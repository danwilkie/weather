package dan.wilkie.weather.forecast;

import java.util.List;

public class GsonForecast {
    private final List<GsonWeather> weather;
    private final GsonMainForecast main;
    private final long dt;

    public GsonForecast(List<GsonWeather> weather, GsonMainForecast main, long dt) {
        this.weather = weather;
        this.main = main;
        this.dt = dt;
    }

    public List<GsonWeather> getWeather() {
        return weather;
    }

    public GsonMainForecast getMain() {
        return main;
    }

    public long getDt() {
        return dt;
    }
}
