package dan.wilkie.weather.forecast;

import java.util.List;

import dan.wilkie.weather.api.WeatherApi;
import rx.Observable;

public class RetrofitForecastService implements ForecastService {
    private static final String LONDON_ID = "2643743";

    private final WeatherApi weatherApi;

    public RetrofitForecastService(WeatherApi weatherApi) {
        this.weatherApi = weatherApi;
    }

    @Override
    public Observable<List<DailyForecast>> getLondonForecast() {
        return weatherApi.getForecast(LONDON_ID)
                .map(response -> new ForecastMapper().toDailyForecasts(response));
    }
}
