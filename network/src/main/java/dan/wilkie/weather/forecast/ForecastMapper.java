package dan.wilkie.weather.forecast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;
import static java.util.Calendar.DAY_OF_WEEK;
import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.LONG;
import static java.util.Locale.UK;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Converts a GsonForecastResponse (network representation) into a list of DailyForecasts (core model representation)
 */
public class ForecastMapper {
    private static List<String> UMBRELLA_WEATHER = asList("Thunderstorm", "Drizzle", "Rain");
    private static String SUNGLASSES_WEATHER = "Clear";

    public List<DailyForecast> toDailyForecasts(GsonForecastResponse forecastResponse) {
        Map<String, List<GsonForecast>> forecastsByDay = mapForecastsByDay(forecastResponse);
        List<DailyForecast> dailyForecasts = new ArrayList<>();
        for (String dayOfWeek : forecastsByDay.keySet()) {
            dailyForecasts.add(toDailyForecast(dayOfWeek, forecastsByDay.get(dayOfWeek)));
        }
        return dailyForecasts;
    }

    private DailyForecast toDailyForecast(String dayOfWeek, List<GsonForecast> gsonForecasts) {
        double dailyMinTemp = Double.MAX_VALUE;
        double dailyMaxTemp = Double.MIN_VALUE;
        boolean umbrellaNeeded = false;
        boolean sunglassesNeeded = false;

        List<ThreeHourForecast> forecasts = new ArrayList<>();

        for (GsonForecast gsonForecast : gsonForecasts) {
            GsonWeather weather = gsonForecast.getWeather().get(0);
            double temp = gsonForecast.getMain().getTemp();
            double maxTemp = gsonForecast.getMain().getTempMax();
            double minTemp = gsonForecast.getMain().getTempMin();

            if (maxTemp > dailyMaxTemp) {
                dailyMaxTemp = maxTemp;
            }
            if (minTemp < dailyMinTemp) {
                dailyMinTemp = minTemp;
            }
            if (isDuringWorkingHours(gsonForecast.getDt())) {
                if (UMBRELLA_WEATHER.contains(weather.getMain())) {
                    umbrellaNeeded = true;
                }
                if (SUNGLASSES_WEATHER.equals(weather.getMain())) {
                    sunglassesNeeded = true;
                }
            }
            forecasts.add(new ThreeHourForecast(temp, weather.getId(), weather.getMain(), weather.getDescription(), weather.getIcon(), gsonForecast.getDt()));
        }
        return new DailyForecast(dayOfWeek, dailyMinTemp, dailyMaxTemp, forecasts, umbrellaNeeded, sunglassesNeeded);
    }

    private Map<String, List<GsonForecast>> mapForecastsByDay(GsonForecastResponse forecastResponse) {
        Map<String, List<GsonForecast>> forecastsByDay = new LinkedHashMap<>();
        for (GsonForecast forecast : forecastResponse.getList()) {
            String dayOfWeek = toDayOfWeek(forecast.getDt());
            if (!forecastsByDay.containsKey(dayOfWeek)) {
                forecastsByDay.put(dayOfWeek, new ArrayList<>());
            }
            forecastsByDay.get(dayOfWeek).add(forecast);
        }
        return forecastsByDay;
    }

    private String toDayOfWeek(long timestamp) {
        // Since the app only supports London right now, this is hardcoded to UK
        Calendar calendar = Calendar.getInstance(UK);
        calendar.setTimeInMillis(SECONDS.toMillis(timestamp));
        return calendar.getDisplayName(DAY_OF_WEEK, LONG, UK);
    }

    private boolean isDuringWorkingHours(long timestamp) {
        // Since the app only supports London right now, this is hardcoded to UK
        Calendar calendar = Calendar.getInstance(UK);
        calendar.setTimeInMillis(SECONDS.toMillis(timestamp));
        int hourOfDay = calendar.get(HOUR_OF_DAY);
        return hourOfDay > 8 && hourOfDay < 20;
    }
}
