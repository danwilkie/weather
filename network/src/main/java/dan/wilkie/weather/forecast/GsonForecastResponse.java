package dan.wilkie.weather.forecast;

import java.util.List;

public class GsonForecastResponse {

    private final List<GsonForecast> list;

    public GsonForecastResponse(List<GsonForecast> list) {
        this.list = list;
    }

    public List<GsonForecast> getList() {
        return list;
    }
}
