package dan.wilkie.weather.api;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import dan.wilkie.weather.common.RxThreadCallAdapterFactory;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.google.gson.FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES;
import static okhttp3.logging.HttpLoggingInterceptor.Level.BODY;

public class RetrofitFactory {
    private static final String BASE_URL = "http://api.openweathermap.org/";
    private final String baseUrl;

    public RetrofitFactory() {
        this(BASE_URL);
    }

    public RetrofitFactory(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public Retrofit createRetrofit(RxThreadCallAdapterFactory rxThreadCallAdapterFactory) {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(BODY))
                .addInterceptor(new ParamInterceptor())
                .build();

        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(LOWER_CASE_WITH_UNDERSCORES)
                .create();

        return new Retrofit.Builder()
                .client(client)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(rxThreadCallAdapterFactory)
                .build();
    }

    private static class ParamInterceptor implements Interceptor {
        private static final String APP_ID = "APPID";
        private static final String API_KEY = "9ab980154b2797d68656609717cbb4e6";
        private static final String UNITS = "units";
        private static final String METRIC = "metric";

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            HttpUrl url = request.url().newBuilder()
                    .addQueryParameter(APP_ID, API_KEY)
                    .addQueryParameter(UNITS, METRIC)
                    .build();
            request = request.newBuilder().url(url).build();
            return chain.proceed(request);
        }
    }
}
