package dan.wilkie.weather.api;

import dan.wilkie.weather.forecast.GsonForecastResponse;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface WeatherApi {
    @GET("/data/2.5/forecast")
    Observable<GsonForecastResponse> getForecast(@Query("id") String cityId);
}
