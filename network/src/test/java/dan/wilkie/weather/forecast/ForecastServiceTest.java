package dan.wilkie.weather.forecast;

import com.squareup.okhttp.mockwebserver.MockWebServer;
import com.squareup.okhttp.mockwebserver.RecordedRequest;

import org.junit.Test;

import java.util.List;

import dan.wilkie.weather.api.RetrofitFactory;
import dan.wilkie.weather.api.WeatherApi;
import dan.wilkie.weather.common.RxThreadCallAdapterFactory;
import retrofit2.Retrofit;
import rx.observers.TestSubscriber;

import static dan.wilkie.weather.common.MockWebServerBuilder.aMockWebServer;
import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertThat;
import static rx.schedulers.Schedulers.immediate;

public class ForecastServiceTest {
    @Test
    public void parsesJsonForLondonForecast() throws InterruptedException {
        TestSubscriber<List<DailyForecast>> subscriber = TestSubscriber.create();
        MockWebServer mockServer = aMockWebServer().returningJson(json()).start();
        ForecastService forecastService = new RetrofitForecastService(createApi(mockServer));

        forecastService.getLondonForecast().subscribe(subscriber);

        verifyCorrectEndpointWasHit(mockServer);
        subscriber.assertValue(expectedResponse());
    }

    private WeatherApi createApi(MockWebServer mockServer) {
        RxThreadCallAdapterFactory adapter = new RxThreadCallAdapterFactory(immediate(), immediate());
        Retrofit retrofit = new RetrofitFactory(mockServer.url("/").toString()).createRetrofit(adapter);
        return retrofit.create(WeatherApi.class);
    }

    private List<DailyForecast> expectedResponse() {
        return asList(
                new DailyForecast(
                        "Thursday",
                        5.0,
                        6.0,
                        asList(new ThreeHourForecast(5.0, 500, "Rain", "light rain", "10n", 1493326800)),
                        false,
                        false),
                new DailyForecast(
                        "Friday",
                        6.0,
                        10.0,
                        asList(new ThreeHourForecast(9.0, 800, "Clear", "clear sky", "01n", 1493337600),
                                new ThreeHourForecast(6.0, 802, "Clouds", "scattered clouds", "03n", 1493348400)),
                        false,
                        false)
        );
    }

    public String json() {
        return "{\"cod\":\"200\",\"message\":0.0266,\"cnt\":3,\"list\":[\n" +
                "{\"dt\":1493326800,\"main\":{\"temp\":5.0,\"temp_min\":5.0,\"temp_max\":6.0,\"pressure\":1021.47,\"sea_level\":1029.28,\"grnd_level\":1021.47,\"humidity\":96,\"temp_kf\":-0.61},\"weather\":[{\"id\":500,\"main\":\"Rain\",\"description\":\"light rain\",\"icon\":\"10n\"}],\"clouds\":{\"all\":48},\"wind\":{\"speed\":2.56,\"deg\":324.504},\"rain\":{\"3h\":0.7},\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2017-04-28 03:00:00\"},\n" +
                "{\"dt\":1493337600,\"main\":{\"temp\":9.0,\"temp_min\":9.0,\"temp_max\":10.0,\"pressure\":1022.2,\"sea_level\":1029.89,\"grnd_level\":1022.2,\"humidity\":92,\"temp_kf\":-0.41},\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01n\"}],\"clouds\":{\"all\":76},\"wind\":{\"speed\":2.32,\"deg\":337.5},\"rain\":{\"3h\":0.0049999999999999},\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2017-04-28 06:00:00\"},\n" +
                "{\"dt\":1493348400,\"main\":{\"temp\":6.0,\"temp_min\":6.0,\"temp_max\":7.0,\"pressure\":1022.95,\"sea_level\":1030.67,\"grnd_level\":1022.95,\"humidity\":82,\"temp_kf\":-0.2},\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scattered clouds\",\"icon\":\"03n\"}],\"clouds\":{\"all\":24},\"wind\":{\"speed\":2.61,\"deg\":330.504},\"rain\":{},\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2017-04-28 09:00:00\"}\n" +
                "],\"city\":{\"id\":2643743,\"name\":\"London\",\"coord\":{\"lat\":51.5085,\"lon\":-0.1258},\"country\":\"GB\"}}";
    }

    private void verifyCorrectEndpointWasHit(MockWebServer server) throws InterruptedException {
        RecordedRequest request = server.takeRequest();
        assertThat(request.getPath(), startsWith("/data/2.5/forecast?id=2643743"));
    }
}