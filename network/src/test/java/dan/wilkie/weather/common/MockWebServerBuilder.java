package dan.wilkie.weather.common;

import com.squareup.okhttp.mockwebserver.MockResponse;
import com.squareup.okhttp.mockwebserver.MockWebServer;

import java.io.IOException;

public class MockWebServerBuilder {
    private final MockWebServer mockWebServer;

    private MockWebServerBuilder() {
        this.mockWebServer = new MockWebServer();
    }

    public static MockWebServerBuilder aMockWebServer() {
        return new MockWebServerBuilder();
    }

    public MockWebServerBuilder returningJson(String json) {
        return returningJson(json, 200);
    }

    public MockWebServerBuilder returningJson(String json, int statusCode) {
        MockResponse response = new MockResponse()
                .setBody(json)
                .setResponseCode(statusCode)
                .setHeader("Content-Type", "application/json");
        mockWebServer.enqueue(response);
        return this;
    }

    public MockWebServer start() {
        try {
            mockWebServer.start();
        } catch (IOException e) {
            throw new RuntimeException("Failed to start mock webserver.", e);
        }
        return mockWebServer;
    }
}
